﻿using ConsumindoApiVeiculo.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace ConsumindoApiVeiculo
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://senacao.tk/objetos/veiculo_array_objeto";

            Carro Carro = BuscarVeiculo(url);

            Console.WriteLine(string.Format("Marca: {0} - Modelo: {1}  - Ano: {2} - Quilometragem {3}", Carro.Marca, Carro.Modelo, Carro.Ano, Carro.quilometragem));

            foreach (String carro in Carro.Opcionais)
            {
                Console.WriteLine(carro);
            }
            
           
            Console.WriteLine("\n\n");

            Console.WriteLine(string.Format("Nome: {0} - Idade: : {1}  - Celular: {2} - Cidade: {3}", Carro.Vendedor.Nome, Carro.Vendedor.Idade, Carro.Vendedor.Celular, Carro.Vendedor.Cidade));
                     
            Console.ReadLine();
        }

        public static Carro BuscarVeiculo(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);

            Console.WriteLine("\n\n");
            Carro Carro = JsonConvert.DeserializeObject<Carro>(content);

            return Carro;

        }
          
            
       
    }
}
