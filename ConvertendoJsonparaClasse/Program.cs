﻿using ConsumindoApiVeiculo.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace ConsumindoApiVeiculo
{
    class Program
    {
        static void Main(string[] args)
        {
            // url base da API
            string url = "http://senacao.tk/objetos/computador_array_objeto";

            // consumindo API
            Computador Computador = BuscarComputador(url);
            

            Console.WriteLine(string.Format("Marca: {0} - Modelo: {1}  - Memoria: {2} - SSD {3}", Computador.Marca, Computador.Modelo, Computador.Memoria, Computador.SSD));
            //Console.WriteLine(Computador.Softwares);
            foreach(String software in Computador.Softwares)
            {
                Console.WriteLine(software);
            }
            Console.WriteLine("\n\n");
            Console.WriteLine(string.Format("rsocial: {0} - Telefone: {1}  - Endereço: {2}", Computador.Fornecedor.RSocial, Computador.Fornecedor.Telefone, Computador.Fornecedor.Endereco));
            


            Console.ReadLine();
        }
        //Função destinada a consumir a API
        public static Computador BuscarComputador(string url)
        {
            //Trocar pra void
            //Instanciando WebClient para chamadas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);

            Console.WriteLine("\n\n");
            //Não fazer
            Computador Computador = JsonConvert.DeserializeObject<Computador>(content);
            

            return Computador;

        }
        
    }
}
