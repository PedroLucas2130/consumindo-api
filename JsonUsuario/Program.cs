﻿using JsonUsuario.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace JsonUsuario
{
    class Program
    {
        static void Main(string[] args)
        {
            // url base da API
            string url = "http://senacao.tk/objetos/usuario";

            // consumindo API
            Usuarios Usuarios = BuscarUsuarios(url);


            Console.WriteLine(string.Format("Nome: {0} - Email: {1}  - Telefone: {2}", Usuarios.Nome, Usuarios.Email, Usuarios.Telefone));
            //Console.WriteLine(Computador.Softwares);
            foreach (String conhecimentos in Usuarios.Conhecimentos)
            {
                Console.WriteLine(conhecimentos);
            }
            Console.WriteLine("\n\n");
            Console.WriteLine(string.Format("Rua: {0} - Número: {1}  - Bairro: {2} - Cidade: {3} - UF: {4}", Usuarios.Endereco.Rua, Usuarios.Endereco.Numero, Usuarios.Endereco.Bairro, Usuarios.Endereco.Cidade, Usuarios.Endereco.Uf));



            Console.ReadLine();
        }
        //Função destinada a consumir a API
        public static Usuarios BuscarUsuarios(string url)
        {
            //Trocar pra void
            //Instanciando WebClient para chamadas HTTP
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);

            Console.WriteLine("\n\n");
            //Não fazer
            Usuarios Usuarios = JsonConvert.DeserializeObject<Usuarios>(content);


            return Usuarios;

        }

    }
    
}
